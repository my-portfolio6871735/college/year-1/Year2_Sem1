public class Rectangle {

    private double width;

    private double height;

    public Rectangle(){
        width = 1;
        height = 1;
    }

    public Rectangle(double tempWidth, double tempHeight){
        width = tempWidth;
        height = tempHeight;
    }

    public double getWidth(){
       return width;
    }
    public double getHeight(){
        return height;
    }

    public double getArea(){
        double area = width*height;
        return area;
    }

    public double getPerimeter(){
        double perimeter = 2*height+2*width;
        return perimeter;
    }
}

public class TestRectangle {
    public static void main(String args[]){
        Rectangle rectangleA = new Rectangle();
        System.out.println("Width = " + rectangleA.getWidth());
        System.out.println("Height = " + rectangleA.getHeight());
        System.out.println("Area = " + rectangleA.getArea());
        System.out.println("Perimeter = " + rectangleA.getPerimeter());

        Rectangle rectangleB = new Rectangle(4,5.5);
        System.out.println("Width = " + rectangleA.getWidth());
        System.out.println("Height = " + rectangleA.getHeight());
        System.out.println("Area = " + rectangleB.getArea());
        System.out.println("Perimeter = " + rectangleB.getPerimeter());

    }
}


public class Stock {

    private String symbol;
    private String name;
    private double previousClosingPrice;
    private double currentPrice;

    public Stock(String tempName, String tempSymbol) {
        name = tempName;
        symbol = tempSymbol;
    }

    public String getName(){
        return name;
    }

    public String getSymbol(){
        return symbol;
    }

    public double getChangePercent(double prevPrice, double newPrice ){
        previousClosingPrice = prevPrice;
        currentPrice = newPrice;
        double changePercent = (((currentPrice - previousClosingPrice)/(previousClosingPrice))* 100);
        return changePercent;
    }
}


public class TestStock {
    public static void main(String args[]){
        Stock newStock = new Stock("Limerick Software Solutions", "LKSS");
        System.out.println(newStock.getName());
        System.out.println(newStock.getSymbol());

        System.out.println(newStock.getChangePercent(79.45,79.65));
    }
}
