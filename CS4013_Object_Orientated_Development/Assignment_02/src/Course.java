/**
 * LAB 2
 *
 * @author Kevin Collins
 * @version 16.0.2
 * @since 29-09-2022
 */
public class Course {
    /**
     * Creates a database for the name of the course
     */
    private String courseName;
    /**
     * Creates array of length 4 that take string param
     */
    private String[] students = new String[4];
    /**
     * Creates database for number of students
     */
    private int numberOfStudents;

    /**
     * takes in coursename and refers to current obj in method
     * @param courseName
     */
    public Course(String courseName) {
        this.courseName = courseName;
    }

    /**
     * adds a student to the array
     * if teh size if bigger than 100 calls resize method
     * @param student
     */
    public void addStudent(String student) {
        students[numberOfStudents] = student;
        numberOfStudents++;
        if(students.length == 100){
            resize(students);
        }
    }

    /**
     * @return students the names of students
     */
    public String[] getStudents() {
        return students;
    }

    /**
     * @return numberOfStudents gets number of students
     */
    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    /**
     * @returns courseName the courses names
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * drops a student out of the array
     * @param student
     */
    public void dropStudent(String student) {


        for(int i = 0; i  < numberOfStudents; i++){
            if(student == students[i]){
                System.arraycopy(students, i + 1, students, i, students.length - i - 1);
                numberOfStudents--;
            }
        }
    }

    /**
     * doubles the size of the array
     * @param array
     */
    private void resize(String[] array){

        String[] temp = new String[2*array.length];
        System.arraycopy(array,0,temp,0,array.length-1);
        array = temp;
    }
}
