import java.util.Arrays;

public class TestCourse{
    public static void main(String args[]){
        Course courseA = new Course("LM121");

        courseA.addStudent("Tom");
        courseA.addStudent("Dick");
        courseA.addStudent("Harry");

        //System.out.println(courseA.getNumberOfStudents());

        courseA.dropStudent("Harry");
        //courseA.dropStudent("Tom");

        //System.out.println(courseA.getNumberOfStudents());

        System.out.println(courseA.getCourseName());
        System.out.println(Arrays.toString(courseA.getStudents()));
    }
}