public class TestMyPoint {
    public static void main(String args[]){

        MyPoint pointA = new MyPoint();
        System.out.print(pointA.getX() + ",");
        System.out.println(pointA.getY());

        MyPoint pointB = new MyPoint(3,6);
        System.out.print(pointB.getX() + ",");
        System.out.println(pointB.getY());

        System.out.println("Distance from point A to point B = " + pointA.distance(pointB));
        System.out.println("Distance from point A to point C(2,3) = " + pointA.distance(2,3));
    }
}
