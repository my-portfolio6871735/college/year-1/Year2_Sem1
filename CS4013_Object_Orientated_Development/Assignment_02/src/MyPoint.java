public class MyPoint {

    private double x ;
    private double y ;

    //no arg
    public MyPoint(){
        x =0;
        y=0;
    }

    //constructor
    public MyPoint(double newX, double newY){
        x = newX;
        y = newY;
    }

    //getters
    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }

    //distance from one point to set point
    public double distance(MyPoint point){

        double x1 = x;
        double y1 = y;

        double x2 = point.getX();
        double y2 = point.getY();

        double distance = Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
        return distance;
    }

    //distance from one point to set point
    public double distance(double x2, double y2){

        double x1 = x;
        double y1 = y;

        double distance = Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
        return distance;
    }



}
