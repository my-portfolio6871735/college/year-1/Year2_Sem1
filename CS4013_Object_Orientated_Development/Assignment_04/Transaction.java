import java.time.LocalDateTime;

public class Transaction {
    //private LocalDateTime date = new LocalDateTime();
    private double amount, balance;
    private String type, description;

    public Transaction(String type, double amount, double balance, String description){
        this.amount = amount;
        this.balance = balance;
        this.type = type;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Transaction" +
                ": type: " + type +
                ", amount: " + amount +
                ", balance: " + balance +
                ", description: " + description + "\n" ;
    }
}
