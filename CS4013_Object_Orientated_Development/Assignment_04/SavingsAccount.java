
public class SavingsAccount extends BankAccount{

    public SavingsAccount(String name, int id, double balance) {
        super(name, id, balance);
    }

    public void withdraw(double withdrawAmt){
        double balance = getBalance();
        if(balance >= withdrawAmt){
            balance -= withdrawAmt;

            mutateBalance(balance);
            Transaction newTransaction = new Transaction("w",withdrawAmt, balance, "Withdrawel");
            transactions.add((transactions.size()), newTransaction);
        }else{
            System.out.println("Not enough funds in account for this transaction");
            System.out.println();
        }
    }

    @Override
    public String toString() {
        String output = String.format("Bank Account : id " + getId() + ": Balance = € %.2f \n", getBalance());
        return output;
    }
}

