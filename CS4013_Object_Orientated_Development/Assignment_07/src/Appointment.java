import java.util.StringTokenizer;

public class Appointment
{ 
   private String description;
   private AppointmentDate day;
   private AppointmentTime from;
   private AppointmentTime to;

   public Appointment(String token) {
      StringTokenizer tokenizer = new StringTokenizer(token); //
      int descCount = tokenizer.countTokens() - 8; //count tokens - 8 (as 8 tokens in total)
      description = tokenizer.nextToken();
      description += " ";
      for (int i = 0; i < descCount; i++) {
         description += tokenizer.nextToken();
         if (i < descCount) {
            description += " ";
         }
      }

      day = new AppointmentDate(tokenizer.nextToken());
      from = new AppointmentTime(tokenizer.nextToken());
      to = new AppointmentTime(tokenizer.nextToken());
   }
   public boolean equals(Object obj)
   {
      if (obj == null) return false;
      Appointment token = (Appointment) obj;
      return (description.equals(token.description) && day.equals(token.day) && from.equals(token.from) && to.equals(token.to));
   }

   public boolean fallOnDate(AppointmentDate date)
   {
      return day.equals(day);
   }

   public Object getDay() {
        return day;
   }


   public String format() {
      return description + " " + day + " " + from + " " + to; )
   }
}
