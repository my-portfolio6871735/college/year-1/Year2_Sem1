import java.util.Objects;
import java.util.StringTokenizer;

public class AppointmentTime {

    private int hours;
    private int minutes;


    public AppointmentTime(String nextToken) {
        StringTokenizer tokenizer = new StringTokenizer(nextToken); // split the string into tokens
        hours = Integer.parseInt(tokenizer.nextToken(":"));
        minutes = Integer.parseInt(tokenizer.nextToken(":"));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        AppointmentTime token = (AppointmentTime) obj;
        return (hours == token.hours && minutes == token.minutes);

    }

    public String toString() {
        String set = hours + ":";
        if (minutes < 10)
            set += "0" + minutes;
        return set;
    }
}

