import java.io.IOException;


public class AppointmentSystem
{  
   public static void main(String[] args)
        throws IOException
   { 
      AppointmentMenu menu = new AppointmentMenu();
      menu.run();
   }
}
