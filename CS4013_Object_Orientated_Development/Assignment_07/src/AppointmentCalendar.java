import java.util.ArrayList;
import java.util.StringTokenizer;

/**
   An appointment calendar.
*/
public class AppointmentCalendar {



   private ArrayList<Appointment> appointments;

   public AppointmentCalendar() {
      appointments = new ArrayList<Appointment>();
   }


   public void add(Appointment a) {
      appointments.add(a); //adds to arrylist of appoitments
   }

   public void cancel(Appointment a) {
      for (int i = 0; i < appointments.size(); i++) {
         if (appointments.get(i).equals(a)) {
            appointments.remove(i);
         }
         return;
      }
   }

   public ArrayList<Appointment> getAppointmentsForDay(AppointmentDate day) {//get appointments for a day
      ArrayList<Appointment> result = new ArrayList<Appointment>();
      for (Appointment a : appointments) { //
         if (a.getDay().equals(day)) {
            result.add(a);
         }
      }
      return result;
   }

}


