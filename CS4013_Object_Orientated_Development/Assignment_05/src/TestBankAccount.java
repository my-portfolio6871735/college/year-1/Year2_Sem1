
import java.util.ArrayList;
public class TestBankAccount {

    public static void main(String args[]){

        /* BankAccount kevin = new BankAccount(1,1000.00);
        SavingsAccount paul = new SavingsAccount(2,10000.00);
        CurrentAccount sean = new CurrentAccount(3,100.00);

        System.out.println(kevin.toString());

        System.out.println("Added 1000");
        kevin.deposit(1000);
        System.out.println(kevin.toString());


        paul.withdraw(100000.00);
        paul.withdraw(1000.00);
        System.out.println(paul.toString());

        sean.setOverdraft(20.00);
        sean.withdraw(110.00);
        System.out.println(sean.toString()); */


        ArrayList<BankAccount> bankAccountA = new ArrayList<>();

        bankAccountA.add(new CurrentAccount("John" , 0,2000.00));
        bankAccountA.add(new CurrentAccount("Paul", 1, 12000.00));
        bankAccountA.add(new SavingsAccount("Roger", 2,100.00));
        bankAccountA.add(new SavingsAccount("Andrew",3,10.00));

        bankAccountA.get(0).mutateAnnualInterestRate(12.5);

        //adds overdraft of 1000
        //results expected 500.00
        ((CurrentAccount)(bankAccountA.get(0))).setOverdraft(1000.00);
        (bankAccountA.get(0)).withdraw(100);
        (bankAccountA.get(0)).withdraw(2100);
        (bankAccountA.get(0)).withdraw(100);
        System.out.println("Bank Account name: " + bankAccountA.get(0).getName() +", Bank Account id: " + bankAccountA.get(0).getId() + " : Transaction History: \n " + (bankAccountA.get(0).getTransactions().toString()) + "\n");

        (bankAccountA.get(1)).deposit(200);
        (bankAccountA.get(1)).withdraw(10000);
        (bankAccountA.get(1)).deposit(8000.00);
        System.out.println("Bank Account name: " + bankAccountA.get(1).getName() + ", Bank Account id: " + bankAccountA.get(1).getId() + " : Transaction History: \n " + (bankAccountA.get(1).getTransactions().toString()) + "\n");


        //withdraws 1000
        //expected result - Not enough funds
        (bankAccountA.get(2)).withdraw(1000);

        System.out.printf(bankAccountA.toString());





    }
}
