public class CurrentAccount extends BankAccount{

    double balance = getBalance();
    private double overdraw;

    public CurrentAccount(String name, int id, double balance) {
        super(name, id, balance);
    }

    public void setOverdraft(double overdraw) {
        this.overdraw = overdraw;
    }

    public void withdraw(double withdrawAmt){

        if(balance + overdraw >= withdrawAmt){

            super.withdraw(withdrawAmt);

        }else{
            System.out.println("You cannot make this transaction as you have exceeded your overdraw limit");
            System.out.println();
        }
    }

    @Override
    public String toString() {
        String output = String.format("Bank Account : id " + getId() + ": Balance = € %.2f \n", getBalance());
        return output;
    }
}





