import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class BankAccount {

    //datafields
    private int id;
    private double balance;
    private double annualInterestRate;
    private LocalDateTime dateCreated;
    private String name;
    ArrayList<Transaction> transactions = new ArrayList<Transaction>();

    //constructor w variable id, balance
    /*public BankAccount(int id, double balance){
        this.id = id;
        this.balance = balance;
    }*/

    public BankAccount(String name, int id, double balance){
        this.name = name;
        this.id = id;
        this.balance = balance;
    }


    //getters

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public int getId() {
        return id;
    }

    public String getName(){
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public double getAnnualInterestRate() {
        return annualInterestRate;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    //mutators
    public int mutateId(int id){
        this.id = id;
        return this.id;
    }

    public String mutateName(String name){
        this.name = name;
        return this.name;
    }

    public double mutateBalance(double balance){
        this.balance = balance;
        return this.balance;
    }

    public double mutateAnnualInterestRate (double annualInterestRate){
        this.annualInterestRate = annualInterestRate;
        return this.annualInterestRate;
    }

    public double getMonthlyInterestRate(){
        double monthlyInterestRate;
        monthlyInterestRate = annualInterestRate/12;
        return monthlyInterestRate;
    };

    public void withdraw(double withdrawAmt){
        balance -= withdrawAmt;
        Transaction newTransaction = new Transaction("w",withdrawAmt, balance, "Withdrawal");
        transactions.add((transactions.size()), newTransaction);
    }
    public void deposit(double depositAmt){
        balance += depositAmt;
        Transaction newTransaction = new Transaction("d",depositAmt, balance, "Deposit");
        transactions.add((transactions.size()), newTransaction);
    }

    @Override
    public String toString() {
        String output = String.format("Bank Account : id " + getId() + ": Balance = € %.2f \n" , getBalance());
        return output;
    }
}

