public class StudentResult {

    private String studentId;
    private TestResult result = new TestResult();

    //constructor with param studentID
    public StudentResult(String studentId){
        this.studentId = studentId;
    }

    //constructor with param studentID, number of tests
    public StudentResult(String studentId, int numberOfTests){
        this.studentId = studentId;
    }

    //getter for student id
    public String getStudentId() {
        return studentId;
    }

    public void addResult(int testId, double value){

        result.setScore(testId,value);
    }

    public String toString(){
        String results;
        results = studentId + " , " + result.getTotal() + " , " + result.getGrade() + "\n";

        return results;
    }


}
