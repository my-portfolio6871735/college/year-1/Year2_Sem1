import java.util.ArrayList;

public class GradeBook {

    private String name;
    private int numberOfTests;

    ArrayList<String> studentIds = new ArrayList<>();
    ArrayList<StudentResult> gradeBookObj = new ArrayList<>();

    public GradeBook(String name, int numberOfTest){
        this.name = name;
        this.numberOfTests = numberOfTest;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfTests() {
        return numberOfTests;
    }

    void addStudentResult (String id, int testId, double value){
        int i = 0;

        if(studentIds.contains(id)){
            i = studentIds.indexOf(id);
            (gradeBookObj.get(i)).addResult(testId, value);

        }else{
            studentIds.add(id);
            StudentResult newGradeBook = new StudentResult(id);
            newGradeBook.addResult(testId, value);
            gradeBookObj.add((studentIds.indexOf(id)),newGradeBook);
        }
    }

    public String toString(){
        String tempresults = "";
        String results = "";
        int i = 0;
        for (int j = studentIds.size(); j>0; j-- ){

            tempresults = "Student : " + gradeBookObj.get(i).toString() + "\n" ;
            results = results + tempresults ;
            i++;
        }

        return results;
    }

}