#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

pthread_mutex_t lock;  // Declare the mutex object

void *thread_function(void *arg)
{
    pthread_mutex_lock(&lock);  // Acquire the lock

    // Critical section of code goes here
    printf("Thread %d is executing the critical section.\n", (int)arg);
    sleep(1);  // Sleep for 1 second to simulate work

    pthread_mutex_unlock(&lock);  // Release the lock

    return NULL;
}

int main(void)
{
    pthread_t thread1, thread2;  // Declare thread variables

    pthread_mutex_init(&lock, NULL);  // Initialize the mutex

    // Create two threads
    pthread_create(&thread1, NULL, thread_function, (void *)1);
    pthread_create(&thread2, NULL, thread_function, (void *)2);

    // Wait for the threads to finish
    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);

    pthread_mutex_destroy(&lock);  // Destroy the mutex

    return 0;
}