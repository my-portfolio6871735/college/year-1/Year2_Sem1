#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "util.h"

int main(int argc, char*argv[]){
  int flipVar = (strcmp(argv[1], "-f") == 0);
  
  char c;
  FILE *from;
  FILE *to;

  int i = 1;
  if (flipVar) {
    i++;
  }

  from = fopen(argv[i], "r");
  to = fopen(argv[i + 1], "w");
  if (from == NULL) {
    perror(argv[i]);
    exit(1);
  }
	     

    while (c = getc(from) != EOF)
      {
	if (flipVar) {
	  putc(flipChar(c), to);
	}else {
	  putc(c, to);
	}
      }
      
    fclose(from);
    fclose(to);
    return 0;
  }
	     
    
	     
