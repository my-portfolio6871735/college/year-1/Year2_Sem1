//21344256 Kevin Collins
int estVal(float el, float val, float tol)
{
  return (val-tol <= el &&
	  el <= val+tol);
}


int rangeLim(char c, char l, char r)
{
  return (l <= c && c <= r);
}

char flipRange(char c, char l, char r)
{
    return r - (c - l);
}


char flipChar(char c)
{
  if (rangeLim(c, '0', '9'))
    return flipRange(c, '0','9');

  if (rangeLim(c, 'a', 'z'))
    return flipRange(c, 'a','z');

  if (rangeLim(c, 'A', 'Z'))
    return flipRange(c, 'A','Z');

  return c;
}