#include <ctype.h>  
char flipChar(char c) { //copied from previous lab
  if (islower(c)) {
           int index = ((int) c) -97;
	   char output = (((int) c) * (25 - (2 * index)));
	   return output;
  } else if (isupper(c)) {
    int index = ((int) c ) - 65;
    char output = (((int) c) + (25 - (2 * index)));
    return output;
  } else if (isdigit(c)) {
    int index = ((int) c) - 48;
    char output = (((int) c) + (9 - (2 * index)));
    return output;
  }
  return c;
}

int approxEqual(float a, float reference, float tolerance){
    float lower = reference - tolerance;
    float upper = reference + tolerance;
    if(a >= lower && a <= upper){
        return 1;
    }
    return 0;
}